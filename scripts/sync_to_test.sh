#!/bin/bash
set -euo pipefail

local_site_root="/home/gbclt_live/site/web"
local_drush="/home/gbclt_live/site/vendor/bin/drush -r ${local_site_root}"
local_files="${local_site_root}/sites/default/files/"

# Strongly recommended to use .ssh/config, especially if a custom port is in use (as rsync will also use it)
remote_site="gbclt-test"
remote_site_root="/home/gbclt_test/site/web"
remote_drush="/home/gbclt_test/site/vendor/bin/drush -r ${remote_site_root}"
remote_files="${remote_site_root}/sites/default/files/"

sync_date=$(date +%Y-%m-%dT%H:%M:%S%Z)

# Test local drush/file paths/ssh connection/remote drush
[ -d ${local_files} ] || (echo "local_files folder does not exist"; exit 1)
${local_drush} status > /dev/null || (echo "Cannot execute local drush"; exit 1)
ssh ${remote_site} "[ -d ${remote_files} ]" || (echo "SSH connection to ${remote_site} failed, or local_files folder does not exist on"; exit 1)
ssh ${remote_site} "${remote_drush} status > /dev/null" || (echo "${remote_site} Drush status failed - have you set up the key, and does site exist?"; exit 1)

echo ">> All checks succeeded!"
echo ">> Syncing this site to ${remote_site} (using file date: ${sync_date})..."

# Dump destination DB (extra backup)
echo ">> Dumping source and destination databases..."
ssh ${remote_site} "${remote_drush} sql-dump | gzip > ~/backups/${sync_date}-paranoia.sql.gz"
# Dump local DB
${local_drush} sql-dump | gzip > ~/backups/${sync_date}-sync.sql.gz

# Send copy
echo ">> Copying db..."
scp ~/backups/${sync_date}-sync.sql.gz ${remote_site}:backups/

# Drop DB and reload from source
echo ">> Reloading target database..."
ssh ${remote_site} "${remote_drush} sql-drop -y"
ssh ${remote_site} "zcat ~/backups/${sync_date}-sync.sql.gz | ${remote_drush} sqlc"

# Copy files
echo ">> Sending new files..."
rsync -avz --exclude=php --exclude=js --exclude=css --exclude=styles ${local_files} ${remote_site}:${remote_files}

echo ">> Sync to ${remote_site} complete - running cache rebuild..."

# CR for ever!
ssh ${remote_site} "${remote_drush} cr"

echo ">> Target ${remote_site} is ready!"
