# Deployment

```bash
cd ~/Projects/drutopia-platform/drutopia_host/hosting_private
ahoy git-pull-all
ahoy deploy-build next
ahoy deploy-site gbclt_test
```

And you can do the same as that last line for `gbclt_live`

***WARNING*** [Drutopia Dev Template](https://www.drupal.org/project/drutopia_dev_template) does *not* have a committed composer.lock file, so you are not guaranteed the same code on deploy as you have in your local.
